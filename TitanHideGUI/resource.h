#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define DLG_MAIN                                100
#define IDC_CHK_NTCLOSE                         1001
#define IDC_CHK_THREADHIDEFROMDEBUGGER          1002
#define IDC_CHK_PROCESSDEBUGFLAGS               1009
#define IDC_CHK_PROCESSDEBUGOBJECTHANDLE        1012
#define IDC_CHK_PROCESSDEBUGPORT                1013
#define IDC_CHK_DEBUGOBJECT                     1014
#define IDC_EDT_PID                             1016
#define IDC_BTN_UNHIDE                          1018
#define IDC_BTN_HIDE                            1019
#define IDC_BTN_UNHIDEALL                       1021
#define IDC_CHK_SYSTEMDEBUGGERINFORMATION       1022
